﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = (int)numericUpDown1.Value;
            dataGridView1.RowCount = n;
            dataGridView1.ColumnCount = n;
            for (int i=0; i < n; i++)
            {
                dataGridView1.Rows[i].HeaderCell.Value = i.ToString();
                dataGridView1.Columns[i].HeaderText = i.ToString();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool[,] matr = new bool[dataGridView1.RowCount, dataGridView1.RowCount];
            for (int i=0; i < dataGridView1.RowCount; i++)
            {
                for (int j=i+1;j< dataGridView1.RowCount; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value != null)
                    {
                        matr[i, j] = true;
                        matr[j, i] = true;
                    }
                    else
                    {
                        matr[i, j] = false;
                        matr[j, i] = false;
                    }
                }
            }
            Solver solver = new Solver(dataGridView1.RowCount, matr);
            if (solver.Solve((int)numericUpDown2.Value, (int)numericUpDown3.Value, 3))
            {
                List<Road> roads = solver.Solution;
                string result = "";
                foreach(Road road in roads)
                {
                    result += $"[{road.From} ; {road.To}], ";
                }
                MessageBox.Show(result);
            }
            else
            {
                MessageBox.Show("Нельзя");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graf
{
    public class Solver
    {
        public bool[,] Matr { get; set; }
        private int Number { get; set; }
        public List<Road> Solution { get; set; } = new List<Road>();
        public Solver(int number, bool[,] matr)
        {
            Number = number;
            Matr = matr;
        }
        public bool Solve(int first, int second, int count)
        {
            if (count == 0)
            {
                return !HasRoad(first, second);
            }
            for (int i=0; i < Number; i++)
            {
                for (int j = i+1; j < Number; j++)
                {
                    if (Matr[i, j])
                    {
                        Matr[i, j] = false;
                        Matr[j, i] = false;
                        Road road = new Road { From = i, To = j };
                        Solution.Add(road);
                        if (Solve(first, second, count - 1))
                        {
                            return true;
                        }
                        Matr[i, j] = true;
                        Matr[j, i] = true;
                        Solution.Remove(road);
                    }
                }
            }
            return false;
        }
        private bool HasRoad(int first, int second)
        {
            HashSet<int> visited = new HashSet<int>();
            return HasRoad(first, second, visited);
        }
        private bool HasRoad(int first, int second, HashSet<int> visited)
        {
            if (Matr[first, second])
            {
                return true;
            }
            else
            {
                visited.Add(first);
                for (int i = 0; i < Number; i++)
                {
                    if (Matr[first, i] && !visited.Contains(i))
                    {
                        if (HasRoad(i, second, visited))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }
}

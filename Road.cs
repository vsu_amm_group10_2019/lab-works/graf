﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graf
{
    public class Road
    {
        public int From { get; set; }
        public int To { get; set; }
    }
}
